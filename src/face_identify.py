import json
import boto3
rekognition_client = boto3.client('rekognition')

def lambda_handler(event, context):
    
    response = rekognition_client.search_faces_by_image(
        CollectionId='build_face_collection_unicorn',
        Image={
            'S3Object': {
                'Bucket': event['Bucket'],
                'Name': event['ObjectName'],
            
            }
        },
        MaxFaces=2,
        FaceMatchThreshold=60
    )
    
    if len(response['FaceMatches']) == 0:
        print("No Matches!")
        rekognition_face = "This face is not in collection"
    else :
        rekognition_face = str(response['FaceMatches'][0]['Face']['ExternalImageId'])
        print("Rekognition result:"+rekognition_face)
    
    return (rekognition_face)
